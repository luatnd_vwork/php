<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

/*
Given a zero-indexed non-negative integer array, N = size of A.
N in range [1..1000000]
A[i] in range [-2^63, 2^63]

For example: A = Array
(
    [0] => 0
    [1] => 1
    [2] => 3
    [3] => -2
    [4] => 0
    [5] => 1
    [6] => 0
    [7] => -3
    [8] => 2
    [9] => 3
)

We call a `pit` is a (P,Q,R) set that 0 <= P < Q < R < N;
And A[P]..A[Q] is in decreased order
And A[Q]..A[R] is in increased order

FE above array, we have:
(2,3,4) <=> [A[2], A[3]] and [A[3], A[4]] <=> [3,-2] and [-2,0]
(2,3,5) <=> [A[2], A[3]] and [A[3], A[4], A[5]] <=> [3,-2] and [-2,0,1]
...

We call `depth` of the `pit` is:
    depth = min(A[P] - A[Q], A[R] - A[Q])

FE above array, we have:
(2,3,4) <=> depth = min(3 - -2, 0 - -2) = 2
(2,3,5) <=> depth = min(3 - -2, 1 - -2) = 3
(5,7,8) <=> depth = min(1 - -3, 2 - -3) = 4


Question: Find the max depth of all possible `pit` you found.
Input: Array
Output: MaxDepth found or -1 if you not found the max depth.
Expected complexity:
    Time: O(n)
    Space: O(1)
 */

$testCases = [
    0 => [0, 1, 3, -2, 0, 1, 0, -3, 2, 3],      // (2,3,5) + (5,7,9) --> 4
    1 => [9, -2, -1, 1, 3, 3, 3, 2, 3, 3],      // (0,1,4) + (6,7,8) --> 5
    2 => [9, -2, -2, -1, 1, 3, 3, 3, 2, 3, 3],  // (7,8,9) --> 1
    3 => [9, -2, -2, -1, 1, 3, 3, 3, 4, 3, 3],  // Not found --> -1
];

foreach ($testCases as $testCase) {
    //echo "\n". '$testCase: '; echo json_encode($testCase);
    echo "\n". '$testCase: '; print_r($testCase);
    $result = solution($testCase);
    echo "\n" . '$result : '; var_dump($result);
}

function solution($A)
{
    $maxDepth = -1;

    // Find pit and its: PQR, depth: d[P_Q_R] => depth
    $d = getDepthList($A);
    // echo '<pre>$d: '; print_r($d); echo "</pre>";

    // Find max depth
    foreach ($d as $depth) {
        if ($depth > $maxDepth) {
            $maxDepth = $depth;
        }
    }

    return $maxDepth;
}

/**
 * Get list of pit and it's depth
 *
 * After draw a line graph of array, you can see this problem can be:
 *     • Finding the deepest valley created by mountains
 *
 * Complexity:
 *  Time: O(?)
 *  Space: O(?)
 *
 * @param $A
 * @return array [
 *      P_Q_R => depth
 *      P_Q_R => depth
 *      P_Q_R => depth
 *      ]
 */
function getDepthList($A)
{
    $n = count($A);
    $depthList = [];

    $dir = [
        'INC' => 1,
        'EQ'  => 0,
        'DEC' => -1,
    ];

    $directionFlag = [$dir['INC']]; // Assuming start is increase
    $floorIndexes  = [];
    for ($i = 1; $i < $n; $i++) {
        $currDir = ($A[$i] > $A[$i - 1])
            ? $dir['INC']
            : ($A[$i] === $A[$i - 1] ? $dir['EQ'] : $dir['DEC']);

        $directionFlag[$i] = $currDir;
        if ($currDir === $dir['INC'] && $directionFlag[$i - 1] === $dir['DEC']) {
            $floorIndexes[] = $i - 1;
        }
    }

    //echo '<pre>$directionFlag: '; print_r($directionFlag); echo "</pre>";
    //echo '<pre>$floorIndexes: '; print_r($floorIndexes); echo "</pre>";

    // Find the PQR
    foreach ($floorIndexes as $Q) {
        $P = $Q - 1;
        $R = $Q + 1;

        $stopP = false;
        $Ps = [];
        while (!$stopP) {
            pushDepth($depthList, $A, $P, $Q, $R);
            $Ps[] = $P;

            $hasPrev = $directionFlag[$P] === $dir['DEC'] && $directionFlag[$P - 1] !== $dir['DEC'];
            $stopP = ($P === 0) || !($hasPrev);
            $P--;
        }

        $R++;
        $stopR = ($R >= $n) || ($directionFlag[$R] !== $dir['INC']);
        while (!$stopR) {
            foreach ($Ps as $P) {
                pushDepth($depthList, $A, $P, $Q, $R);
            }

            $R++;
            $stopR = ($R >= $n) || ($directionFlag[$R] !== $dir['INC']);
        }
    }


    return $depthList;
}

function pushDepth(&$d, $A, $P, $Q, $R)
{
    $depth = calDepth($A, $P, $Q, $R);
    $k     = encodePQRKey($P, $Q, $R);
    $d[$k] = $depth;

    echo "\n" . 'P_Q_R => $depth: ' . $k . " => " . $depth;
}

function calDepth($A, $P, $Q, $R)
{
    return min($A[$P] - $A[$Q], $A[$R] - $A[$Q]);
}

function encodePQRKey($P, $Q, $R)
{
    return "${P}_${Q}_${R}";
}

function decodePQRKey($PQR)
{
    $s      = explode('_', $PQR);
    $a['P'] = $s[0];
    $a['Q'] = $s[1];
    $a['R'] = $s[2];

    return $a;
}
