<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

/*
Given a zero-indexed non-negative integer array, N = size of A.
N in range [1..1000000]
A[i] in range [-2^63, 2^63]

For example: A = Array
(
    [0] => 0
    [1] => 1
    [2] => 3
    [3] => -2
    [4] => 0
    [5] => 1
    [6] => 0
    [7] => -3
    [8] => 2
    [9] => 3
)

We call a `pit` is a (P,Q,R) set that 0 <= P < Q < R < N;
And A[P]..A[Q] is in decreased order
And A[Q]..A[R] is in increased order

FE above array, we have:
(2,3,4) <=> [A[2], A[3]] and [A[3], A[4]] <=> [3,-2] and [-2,0]
(2,3,5) <=> [A[2], A[3]] and [A[3], A[4], A[5]] <=> [3,-2] and [-2,0,1]
...

We call `depth` of the `pit` is:
    depth = min(A[P] - A[Q], A[R] - A[Q])

FE above array, we have:
(2,3,4) <=> depth = min(3 - -2, 0 - -2) = 2
(2,3,5) <=> depth = min(3 - -2, 1 - -2) = 3
(5,7,8) <=> depth = min(1 - -3, 2 - -3) = 4


Question: Find the max depth of all possible `pit` you found.
Input: Array
Output: MaxDepth found or -1 if you not found the max depth.
Expected complexity:
    Time: O(n)
    Space: O(1)
 */


$testCases = [
    0 => [0, 1, 3, -2, 0, 1, 0, -3, 2, 3],      // (2,3,5) + (5,7,9) --> 4
    1 => [9, -2, -1, 1, 3, 3, 3, 2, 3, 3],      // (0,1,4) + (6,7,8) --> 5
    2 => [9, -2, -2, -1, 1, 3, 3, 3, 2, 3, 3],  // (7,8,9) --> 1
    3 => [9, -2, -2, -1, 1, 3, 3, 3, 4, 3, 3],  // Not found --> -1
];

foreach ($testCases as $testCase) {
    //echo "\n". '$testCase: '; echo json_encode($testCase);
    echo "\n" . '$testCase: '; print_r($testCase);

    $result = solution($testCase);

    echo "\n" . '$result : '; var_dump($result);
}

function solution($A)
{
    $N = count($A);

    $maxDepth = -1;


    $cell     = null; // Left point index
    $prevCell = null; // prev Left point index
    $floor    = null; // Middle point index

    $tmpMaxCell = null;

    $dir = [
        'ASC' => 1,
        'EQ'  => 0,
        'DEC' => -1,
    ];


    for ($i = 0; $i < $N; $i++) {
        $currVal = $A[$i];
        $currDir = getDir($A, $i, $dir);


        if ($i === $N - 1) {
            // This will move cell if this array segment is not decrease
            $isCell = ($currDir === $dir['ASC']) || ($currDir === $dir['EQ']);
            $isFloor = false;
        } else {
            $nextDir = getDir($A, $i + 1, $dir);

            // This will move cell if this array segment is not decrease
            $isCell = ($nextDir === $dir['DEC']) && ($tmpMaxCell <= $currVal);
            $isFloor = ($currDir === $dir['DEC'] && $nextDir === $dir['ASC']);
        }


        if ($isCell) {
            if (is_null($floor)) {
                $cell = $i;
            } else {
                // DO
                $prevCell = $cell;
                $cell = $i;

                $depth    = calcDepth($A, $prevCell, $floor, $cell);
                $maxDepth = max($maxDepth, $depth);
                // END DO
            }

            $tmpMaxCell = $currVal;
        } elseif ($isFloor) {
            $floor = $i;
            $tmpMaxCell = $currVal;
        } else {
            if ($currDir === $dir['ASC']) {
                $tmpMaxCell = $currVal;
            }
        }

        //echo "\n". '$i, $prevCell, $floor, $cell,$tmpMaxCell: '. "\n"; var_dump($i, $prevCell, $floor, $cell, $tmpMaxCell);
    }

    return $maxDepth;
}

function getDir($A, $i, $dir)
{
    return $i === 0
        ? $dir['ASC'] // Assume that the first element is ASC
        : (($A[$i] > $A[$i - 1])
            ? $dir['ASC']
            : ($A[$i] === $A[$i - 1] ? $dir['EQ'] : $dir['DEC']));
}

function calcDepth($A, $P, $Q, $R)
{
    $depth = min($A[$P] - $A[$Q], $A[$R] - $A[$Q]);

    echo "\n" . 'P_Q_R => $depth: ' . sprintf('(%d,%d,%d)', $P, $Q, $R) . " => " . $depth;

    return $depth;
}
