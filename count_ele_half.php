<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$testCases = [
    [
        'in' => [0,1,1,1,2,2,2,2,2],
        'out' => 2
    ],
    [
        'in' => [0,1,1,1,2,2,2,2],
        'out' => -1
    ],
    [
        'in' => [0,0,0,0,1, 1, 1, 1, 1, 1, 1, 1,1,1,7,9,12],
        'out' => 1
    ],
    [
        'in' => [2, 2, 2, 2, 2, 3, 4, 4, 4, 6],
        'out' => -1
    ],
    [
        'in' => [2, 2, 2, 2, 2, 2, 3, 4, 4, 4, 6],
        'out' => 2
    ],
];

foreach ($testCases as $testCase) {
    $in = $testCase['in'];
    echo "\n" . 'INPUT: '; print_r($in);
    //echo "\n". 'INPUT: '; echo json_encode($in);

    //$out = solution($in);
    $out = solutionB($in);
    echo "\n" . 'OUTPUT: '; var_dump($out);
    echo ""   . 'EXPECT: '; var_dump($testCase['out']);
}


/**
 * The source of Problem is `codility.com`
 * Find the wrong section and fix it, fix no more than 3 lines
 *
 * Time: O(n)
 * Space: O(n)
 *
 * @param $A
 * @return int
 */
function solution(&$A) {
    $n = sizeof($A);
    $L = array_pad(array(), $n + 1, -1);
    for ($i = 0; $i < $n; $i++) {
        $L[$i + 1] = $A[$i];
    }
    $count = 0;
    $pos = (int) (($n + 1) / 2);
    $candidate = $L[$pos];
    for ($i = 1; $i <= $n; $i++) {
        if ($L[$i] == $candidate)
            $count = $count + 1;
    }
    // Original begin
    //if ($count > $pos)
    //    return $candidate;
    // Original end
    // Fix begin
    if ($count > $n / 2)
        return $candidate;
    // Fix end

    return (-1);
}

/**
 * https://en.wikipedia.org/wiki/Boyer%E2%80%93Moore_majority_vote_algorithm
 * Time: O(n)
 * Space: O(1)
 *
 * @param $A
 * @return int
 */
function solutionB($A)
{
    $n              = count($A);
    $candidateIndex = 0;
    $count          = 1;

    for ($i = 1; $i < $n; $i++) {
        if ($A[$i] === $A[$candidateIndex]) {
            $count++;
        } else {
            $count--;
        }

        if ($count === 0) {
            $candidateIndex = $i;
        }
    }

    //echo "\n" . '$candidateIndex: '; var_dump($candidateIndex);

    $count     = 0;
    $candidate = $A[$candidateIndex];
    $half      = intval($n / 2);

    foreach ($A as $v) {
        if ($candidate === $v) {
            $count++;
        }
        if ($count > $half) {
            return $A[$candidateIndex];
        }
    }

    if ($count > $half) {
        return $A[$candidateIndex];
    }

    return -1;
}
