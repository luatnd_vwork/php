<?php

/**
 * Smallest Missing integer number in array
 * Worst Time: O(n)
 * Worst Space: O(n)
 */

/*
This is a demo task.

Write a function:

function solution($A);

that, given an array A of N integers, returns the smallest positive integer (greater than 0) that does not occur in A.

For example, given A = [1, 3, 6, 4, 1, 2], the function should return 5.

Given A = [1, 2, 3], the function should return 4.

Given A = [−1, −3], the function should return 1.

Assume that:

N is an integer within the range [1..100,000];
each element of array A is an integer within the range [−1,000,000..1,000,000].
Complexity:

expected worst-case time complexity is O(N);
expected worst-case space complexity is O(N), beyond input storage (not counting the storage required for input arguments).
Elements of input arrays can be modified.
*/

$testCases = [
    0 => [1, 1000, 2, 6, 9, 8, 10, 3, 4, 5],
    1 => [1, 3, 6, 4, 1, 2],
    2 => [1, 2, 3],
    3 => [5, 6, 7, 9, 1, 3],
    4 => [-5, 6, 7, 9, 1, 3],
];
foreach ($testCases as $testCase) {
    var_dump(getSmallestInt($testCase));
}


function getSmallestInt($arr)
{
    $number = 1;

    $hash = buildHash($arr);

    for ($i = 1, $c = count($arr); $i <= $c; $i++) {

        if (!isset($hash[$i])) {
            $number = $i;
            break;
        } else {
            $number = $i + 1;
        }
    }

    return $number;
}

function buildHash($arr)
{
    $hash = [];

    foreach ($arr as $i) {
        if ($i > 0) {
            $hash[$i] = $i;
        }
    }

    return $hash;
}
