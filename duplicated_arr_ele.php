<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

/**
 * Duplicated ele in positive array
 */

$testCases = [
    0 => [1, 1000, 20, 2, 6, 9, 8, 10, 3, 3, 4, 5, 6, 1],
    //1 => [1, 3, 6, 4, 1, 2],
    //2 => [1, 2, 3],
    //3 => [5, 6, 7, 9, 1, 3],
    //4 => [1, 6, 7, 9, 1, 3],
];

foreach ($testCases as $testCase) {
    print_r(getDuplicatedEle($testCase));
}

/**
 * Worst Time: O(n)
 * Worst Space: O(m) | m is number of duplicated
 *
 * Require On so that only need to loop 1 times
 * Require O(1) space so that you can not use another On space to save a hashed of count
 *
 * ==> Output is duplicated or not ? --------> Something like toggle info here: 0/1 , +/-
 * ==> Because array ele is positive > 0  -------> use +/- is the best way to do this
 *
 * @param $a
 * @return array
 */
function getDuplicatedEle($a)
{
    $duplicated = [];

    for ($i = 0, $c = count($a); $i < $c; $i++) {
        // Fallback
        if (!isset($a[abs($a[$i])])) {
            $a[abs($a[$i])] = 1;
        }

        if ($a[abs($a[$i])] >= 0) {
            $a[abs($a[$i])] = -$a[abs($a[$i])];
        } else {
            $duplicated[] = abs($a[$i]);
        }
    }

    return $duplicated;
}
