<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

/*
Given a zero-indexed non-negative integer array, N = size of A.
N in range [1..1000000]
A[i] in range [-2^63, 2^63]

For example: A = Array
(
    [0] => 0
    [1] => 1
    [2] => 3
    [3] => -2
    [4] => 0
    [5] => 1
    [6] => 0
    [7] => -3
    [8] => 2
    [9] => 3
)

We call a `pit` is a (P,Q,R) set that 0 <= P < Q < R < N;
And A[P]..A[Q] is in decreased order
And A[Q]..A[R] is in increased order

FE above array, we have:
(2,3,4) <=> [A[2], A[3]] and [A[3], A[4]] <=> [3,-2] and [-2,0]
(2,3,5) <=> [A[2], A[3]] and [A[3], A[4], A[5]] <=> [3,-2] and [-2,0,1]
...

We call `depth` of the `pit` is:
    depth = min(A[P] - A[Q], A[R] - A[Q])

FE above array, we have:
(2,3,4) <=> depth = min(3 - -2, 0 - -2) = 2
(2,3,5) <=> depth = min(3 - -2, 1 - -2) = 3
(5,7,8) <=> depth = min(1 - -3, 2 - -3) = 4


Question: Find the max depth of all possible `pit` you found.
Input: Array
Output: MaxDepth found or -1 if you not found the max depth.
Expected complexity:
    Time: O(n)
    Space: O(1)
 */


$testCases = [
    0 => [0, 1, 3, -2, 0, 1, 0, -3, 2, 3],      // (2,3,5) + (5,7,9) --> 4
    1 => [9, -2, -1, 1, 3, 3, 3, 2, 3, 3],      // (0,1,4) + (6,7,8) --> 5
    2 => [9, -2, -2, -1, 1, 3, 3, 3, 2, 3, 3],  // (7,8,9) --> 1
    3 => [9, -2, -2, -1, 1, 3, 3, 3, 4, 3, 3],  // Not found --> -1
];

foreach ($testCases as $testCase) {
    //echo "\n". '$testCase: '; echo json_encode($testCase);
    echo "\n" . '$testCase: '; print_r($testCase);

    $result = solution($testCase);

    echo "\n" . '$result : '; var_dump($result);
}

function solution($A)
{
    $N = count($A);

    $maxDepth = -1;


    $maxP = 0; // Left point
    $Q    = null; // Middle point
    $maxR = null; // Right point

    $prevMaxP = $maxP; // Store the MaxP position to calculate the `depth`

    $dir = [
        'ASC' => 1,
        'EQ'  => 0,
        'DEC' => -1,
    ];


    for ($i = 0; $i < $N - 1; $i++) {
        $currDir = getDir($A, $i, $dir);
        $nextDir = getDir($A, $i + 1, $dir);

        $isFloor = ($currDir === $dir['DEC'] && $nextDir === $dir['ASC']);
        if ($isFloor) {
            if (is_null($Q)) {
                // Have max left but have not had $Q and maxR:
                $Q = $i;
                $prevMaxP = $maxP; // maxR is being built
                // $maxR = $i + 1; // Might be redundant: Already update bellow, maxR is being built
            } else {

                // Execute save depth for the old: $maxP, $maxR, $Q; // there's no prev $Q and $maxR info for the first floor found
                $newDepth = calcDepth($A, $prevMaxP, $Q, $maxR);
                $maxDepth = max($maxDepth, $newDepth);

                // Update some info
                $Q = $i;
                $prevMaxP = $maxP; // maxR is being built

                if ($maxP < $maxR) {
                    $maxP = $maxR;
                }
            }

        } else {
            if (is_null($Q)) {
                // When have not found any Floor yet
                // Update max left: when ASC or EQ
                if ($currDir !== $dir['DEC']) {
                    $maxP = $i;
                }
            } else {
                // When found at least 1 Floor:

                // Update max left: when EQ
                if ($currDir === $dir['EQ']) {
                    $maxP = $i;
                }

                // Update max right: when ASC only: Because `pit` is ASC strict
                else if ($currDir === $dir['ASC']) {
                    $maxR = $i;
                }
            }

        }


        //echo "\n". '$i, $maxP, $Q, $maxR: '. "\n"; var_dump($i, $maxP, $Q, $maxR);

    }

    // Update the rest element if exist
    if (!is_null($Q)) {
        $currDir = getDir($A, $i, $dir);
        if ($currDir === $dir['ASC']) {
            $maxR = $i;
        }

        // check the rest floor
        $newDepth = calcDepth($A, $maxP, $Q, $maxR);
        $maxDepth = max($maxDepth, $newDepth);
    }

    return $maxDepth;
}

function getDir($A, $i, $dir)
{
    return $i === 0
        ? $dir['ASC'] // Assume that the first element is ASC
        : (($A[$i] > $A[$i - 1])
            ? $dir['ASC']
            : ($A[$i] === $A[$i - 1] ? $dir['EQ'] : $dir['DEC']));
}

function calcDepth($A, $P, $Q, $R)
{
    $depth = min($A[$P] - $A[$Q], $A[$R] - $A[$Q]);

    echo "\n" . 'P_Q_R => $depth: ' . sprintf('(%d,%d,%d)', $P, $Q, $R) . " => " . $depth;

    return $depth;
}
